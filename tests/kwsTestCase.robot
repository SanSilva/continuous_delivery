**Settings**
Documentation    Kws de Casos de uso.

**Keywords**
Acessar o T.A
    Go To   http://portal.upflow.me/portal/p/uF/task-analyzer
Acessar o Fluig
    loginFluig  http://portal.upflow.me/  fluigadmin  upFlow@2020  chrome

Validar Placeholder inicial
    [Arguments]  ${locatorPlaceholder}=css:#app_placeholder
    Aparecer_Desaparecer   ${locatorPlaceholder}

Acessar o TA em "${idioma}" aguardando o placeholder
    Acessar o Fluig
    fecharAlerta
    Mudar idioma para "${idioma}"
    Acessar o T.A
    Run Keyword And Ignore Error    Validar Placeholder inicial

o tooltip "${tooltip}" esta "${hidden/visible}"
    ${boolean}  Execute Javascript  return $('${tooltip}').css('visibility') == "${hidden/visible}"
    [Return]  ${boolean}

Mudar idioma para "${idioma}"
    ${idioma}  Set Variable if  "${idioma}"=="portugues"    PT_BR
    ...                         "${idioma}"=="ingles"       EN_US  
    ...                         "${idioma}"=="espanhol"     ES
    Wait  css:.menu-main
    Mouse Over  css:.menu-main
    Sleep  5s
    Click   css:.menu-main .dropdown-menu-profile
    Click   css:.dropdown-menu li[data-page-alias="language"]
    Wait    id:WCMLanguage_262
    Click   css:#WCMLanguage_262 li[data-language-val="${idioma}"]