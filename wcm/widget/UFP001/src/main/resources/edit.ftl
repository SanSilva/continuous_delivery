<#assign reposi='UFP001'>
<#assign versao='1.0.0'>
<#assign parametros = "{processos: '${processos!}', nomeProcessos: '${nomeProcessos!}'}">

<script type="text/javascript">const ${reposi}slotId = 3159</script>

<link rel="stylesheet" href="/${reposi}/resources/css/external/bootstrap-duallistbox.min.css" />    
    
<div id="${reposi}_${instanceId}" class="uF ${reposi} super-widget wcm-widget-class fluig-style-guide" 
     data-params="${reposi}.instance({reposi:'${reposi}', versao:'${versao}', widgetId: ${instanceId}, preferences: ${parametros}})">

    <div class="row">
        <div class="col-md-offset-1 col-md-10 col-xs-12 well">

            <div class="page-header">
                <h4><i class="fa fa-pencil" aria-hidden="true"></i> Widget #${instanceId} <small class="hidden-xs">${i18n.getTranslation('widget.edit.info')}</small></h4>
            </div>

            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8" id="msg-free-info"></div>
                <div class="col-md-2"></div>
            </div>
            
            <div class="row" id="Select">
            	<div class="col-md-2"></div>
                <div class="col-md-8">
                    <div class="form-group multiple-select">
						<select multiple="multiple" class='input-select' name="processos" style="height: 250px"></select>	
                    </div>
                </div>
                <div class="col-md-2"></div>
            </div>
            
            <div class="modal-footer" style="padding-bottom:0px;">
                <div class="btn-group">
                    <button type="button" class="btn btn-default" data-login-config-list-itens data-page-alias="wcm-discard-draft" cancelar>${i18n.getTranslation('widget.edit.cancel')}</button>
                    <button type="button" class="btn btn-primary" data-save-preferences>${i18n.getTranslation('widget.edit.save')} ></button>
                </div>
            </div>
        
        </div>
    </div>

</div>

<script src="/${reposi}/resources/js/external/jquery.bootstrap-duallistbox.min.js"></script>

<!--Template de informativo de não licença-->
<script type="text/html" id="tpl-freeinfo">
    <div class="alert alert-info" role="alert">${i18n.getTranslation('widget.edit.free.info')}</div>
</script>

<script type="text/javascript" src="/${reposi}/resources/js/${reposi}.js?v=${versao}" charset="utf-8"></script>