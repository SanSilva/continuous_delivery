robotframework==3.2.2
robotframework-faker==5.0.0
robotframework-seleniumlibrary==4.5.0
requests==2.25.1
requests-oauthlib==1.3.0
robotframework-debuglibrary==2.2.1
discord_webhook==0.11.0