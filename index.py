from discord_webhook import DiscordWebhook, DiscordEmbed
from os import listdir
from os.path import isfile, join
#Pega todos os arquivos da pasta reports
onlyfiles = [f for f in listdir('/opt/robotframework/reports/') if isfile(join('/opt/robotframework/reports/', f))]

#Cria a função que adiciona os arquivos no webhook
def sendfiles(name):
    # send two images
    with open("/opt/robotframework/reports/" + name, "rb") as f:
        webhook.add_file(file=f.read(), filename=name)
        

webhook = DiscordWebhook(url='https://discordapp.com/api/webhooks/768220334188003329/-e83W-WZfAs5r-MkWF0A4C7s-YVXaMHYyCB-6D5_pvZF5ORlniC4_kBYwRb0BTo42en7')

# create embed object for webhook
embed = DiscordEmbed(title='Reports', description='Arquivos de logs robotframework', color=242424)

#Chama o map
result = map(sendfiles,onlyfiles)
send = set(result)

# add embed object to webhook
webhook.add_embed(embed)

response = webhook.execute()